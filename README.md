# Overview

Restic is an open source backup client that runs on Linux. This charm allows
users to build a dynamic and platform wide backup schedule that allows for backup
to Backblaze (more storage providers coming soon).

# Usage

To deploy this charm run:

    juju deploy ~spiculecharms/restic --to <machine>

Because you're backing up specific servers, deploy the charm to existing machines, don't
deploy it to an empty server.

To configure for Backblaze you can do so like this:

```
juju config restic backup-schedule="<valid crontab>"

juju config restic backup-locations="- hostname1:
    testbucketspicule3:
      - /my_backup_loc
      - /my_other_backup2
    testbucketspicule:
      - /my_last_backup
- hostname2:
    testbucketspicule:
      - /location_other"

juju config restic backblaze-account-id=<backblaze id>

juju config restic backblaze-account-key=<backblaze key>
```
Most of the above is pretty straightforward. You can get your id and key from Backblaze, 
the crontab is a schedule in cron format for example: `"0 5 * * *"`.

The backuplocations allows you to specify locations on a per server basis whilst still
deploying a single Restic Charm.

The YAML looks similar to this:
```
- hostname1:
    testbucketspicule3:
      - /my_backup_loc
      - /my_other_backup2
    testbucketspicule:
      - /my_last_backup
- hostname2:
    testbucketspicule:
      - /location_other
```
the hostname keys are the server hostsnames. This ensures each server only backs up what its expecting.
You then define the bucket you want the backup to go in, and then the paths on the server you want backing up.

Finally, you then need to set the restic repository password on the machines, you can either put a file in /root/.restic-pw
or run the action:

    juju run-action restic/1 setpassword password="<my restic password>"

This assumes you've already run an init on the bucket prior to configuring restic. If not run something like:

restic -r b2:my-restic-backup init

on a server with Restic installed


## Known Limitations and Issues

Currently this charm only supports Backblaze, more backup options to come.

The charm should also implement a backup relation for systems that deal with data, to allow charm authors to define the 
sensible backup options for databases etc and have Restic automatically backup the data.

# Contact Information

W: https://www.spicule.co.uk
E: tom@spicule.co.uk
