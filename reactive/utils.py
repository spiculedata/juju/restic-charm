import socket
import yaml


def read_yaml(b2_account_id=None, b2_account_key=None):
    stream = open("test", "r")
    docs = yaml.load(stream)
    b2vars = ""
    if b2_account_id and b2_account_key:
        b2vars = "B2_ACCOUNT_ID=\""+b2_account_id+"\" B2_ACCOUNT_KEY=\""+b2_account_key+"\""
    for docl in docs:
        for doc in docl:
          for k,v in doc.items():
              if socket.gethostname() == k:
                for x,z in v.items():
                  print(b2vars + " RESTIC_REPOSITORY=\"b2:"+x+"\" RESTIC_PASSWORD_FILE=\"restic-pw.txt\" restic backup " + " ".join(z))


read_yaml("1","2")
