from charms.reactive import when, when_not, set_flag
from crontab import CronTab
from charmhelpers.core import hookenv
import yaml
import socket

@when('config.changed')
def update_schedule():
    my_cron = CronTab(user='root')
    if hookenv.config()['backup-schedule'] and hookenv.config()['backup-locations']:
        execs = read_yaml(hookenv.config()['backup-locations'], hookenv.config()['backblaze-account-id'], hookenv.config()['backblaze-account-key'])
        if execs:
            hookenv.log("setting up cron")
            iter = my_cron.find_comment('backblaze')
            for j in iter:
                my_cron.remove( j )
            job = my_cron.new(command=" ".join(execs), comment='backblaze')
            hookenv.log("Schedule "+hookenv.config()['backup-schedule'])
            job.setall(hookenv.config()['backup-schedule'])
            my_cron.write()


def read_yaml(backup_locations, b2_account_id=None, b2_account_key=None):
    hookenv.log("BACKUP LOCATIONS")
    docs = yaml.load_all(backup_locations)
    b2vars = ""
    if b2_account_id and b2_account_key:
        b2vars = "B2_ACCOUNT_ID=\""+b2_account_id+"\" B2_ACCOUNT_KEY=\""+b2_account_key+"\""
    execs = []
    for docl in docs:
        for doc in docl:
          for k,v in doc.items():
              if socket.gethostname() == k:
                for x,z in v.items():
                  execs.append(b2vars + " RESTIC_REPOSITORY=\"b2:"+x+"\" RESTIC_PASSWORD_FILE=\"/root/.restic-pw\" restic backup " + " ".join(z) + " && ")

    last = execs[-1]
    last = last[:-2]
    execs[-1] = last
    return execs


